package com.squad1.mobileconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.mobileconnect.entity.NumberCatalog;

public interface NumberCatalogRepository extends JpaRepository<NumberCatalog, Long> {

}
