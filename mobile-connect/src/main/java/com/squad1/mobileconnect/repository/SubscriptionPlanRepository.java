package com.squad1.mobileconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.mobileconnect.entity.SubscriptionPlan;

public interface SubscriptionPlanRepository extends JpaRepository<SubscriptionPlan, Long>{

}
