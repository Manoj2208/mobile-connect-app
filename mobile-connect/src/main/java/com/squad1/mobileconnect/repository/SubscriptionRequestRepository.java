package com.squad1.mobileconnect.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.mobileconnect.entity.SubscriptionRequest;

public interface SubscriptionRequestRepository extends JpaRepository<SubscriptionRequest, String> {
}
