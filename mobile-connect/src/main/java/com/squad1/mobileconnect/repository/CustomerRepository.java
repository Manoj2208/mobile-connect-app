package com.squad1.mobileconnect.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.squad1.mobileconnect.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	Optional<Customer> findByAadharOrEmail(String aadhar, String email);
}
