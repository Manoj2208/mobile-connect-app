package com.squad1.mobileconnect.kafka_producer;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import com.squad1.mobileconnect.dto.EmailTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ProducerService {
	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(EmailTemplate emailTemplate) {

		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		Producer<String, EmailTemplate> kafkaProducer = new KafkaProducer<>(props, new StringSerializer(),
				new JsonSerializer());

		log.info("Message Sent: " + emailTemplate);
		ProducerRecord<String, EmailTemplate> emailRecord = new ProducerRecord<>("notifier-queue", emailTemplate);
		log.info("" + emailRecord);
		log.info("" + kafkaProducer.send(emailRecord));
	}
}
