package com.squad1.mobileconnect.exception;

public class NumberNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NumberNotFound(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public NumberNotFound() {
		super("Number not found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
