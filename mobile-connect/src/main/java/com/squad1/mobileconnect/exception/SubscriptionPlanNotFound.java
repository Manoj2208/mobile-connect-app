package com.squad1.mobileconnect.exception;

public class SubscriptionPlanNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SubscriptionPlanNotFound(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public SubscriptionPlanNotFound() {
		super("Subscription plan not found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}
}
