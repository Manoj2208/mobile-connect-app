package com.squad1.mobileconnect.exception;

public class NumberAlreadyTaken extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NumberAlreadyTaken(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

	public NumberAlreadyTaken() {
		super("Number Already Taken Please select different one", GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

}
