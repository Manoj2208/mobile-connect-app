package com.squad1.mobileconnect.exception;

public class CustomerConflictExists extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerConflictExists(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}

	public CustomerConflictExists() {
		super("Customer Already Registered", GlobalErrorCode.ERROR_RESOURCE_CONFLICT_EXISTS);
	}
}
