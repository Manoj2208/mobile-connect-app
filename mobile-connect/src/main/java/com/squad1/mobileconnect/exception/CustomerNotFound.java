package com.squad1.mobileconnect.exception;

public class CustomerNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerNotFound(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public CustomerNotFound() {
		super("Customer Not Found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
