package com.squad1.mobileconnect.exception;

public class SubscriptionRequestNotFound extends GlobalException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SubscriptionRequestNotFound(String message) {
		super(message, GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

	public SubscriptionRequestNotFound() {
		super("Subscription request not found", GlobalErrorCode.ERROR_RESOURCE_NOT_FOUND);
	}

}
