package com.squad1.mobileconnect.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad1.mobileconnect.dto.CustomerDto;
import com.squad1.mobileconnect.dto.SubscriptionResponse;
import com.squad1.mobileconnect.service.CustomerService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class CustomerController {
	private final CustomerService customerService;

	@PostMapping("/customers")
	public ResponseEntity<SubscriptionResponse> subscribe(@RequestParam Long subscriptionPlanId,
			@RequestParam Long numberId, @Valid @RequestBody CustomerDto customerDto) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(customerService.subscribePlans(subscriptionPlanId, numberId, customerDto));
	}
}
