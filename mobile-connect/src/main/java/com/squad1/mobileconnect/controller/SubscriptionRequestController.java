package com.squad1.mobileconnect.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.squad1.mobileconnect.dto.ApiResponse;
import com.squad1.mobileconnect.entity.Status;
import com.squad1.mobileconnect.service.SubscriptionRequestService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class SubscriptionRequestController {

	private final SubscriptionRequestService subscriptionRequestService;

	/**
	 * 
	 * @param requestId to verify serviceRequest
	 * @param status to act on  serviceRequest
	 * @return acknowledge on the action on service request
	 */
	@PutMapping("/service-requests/{requestId}")
	public ResponseEntity<ApiResponse> approveServiceRequest(@PathVariable(required = true) String requestId,
			@RequestParam Status status) {
		return ResponseEntity.status(HttpStatus.OK).body(subscriptionRequestService.approveRequest(requestId, status));
	}
}
