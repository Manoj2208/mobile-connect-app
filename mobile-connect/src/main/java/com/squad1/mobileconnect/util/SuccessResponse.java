package com.squad1.mobileconnect.util;

public interface SuccessResponse {

	/**
	 * Success Response
	 */

	String SUCCESS_CODE1 = "SUCCESS01";
	String SUCCESS_MESSAGE1 = "Subscription Requested Successfully";

	String SUCCESS_CODE2 = "SUCCESS02";
	String SUCCESS_MESSAGE2 = "Responded on Service Request successfully";

	String SUCCESS_MESSAGE3 = "Current status of requested Subscription";
	String SUCCESS_CODE3 = "SUCCESS03";

	String SUBJECT = "Request Acknowledgement about Xyz Subscription ";
	String BODY= "Dear Customer Your Request for the subscription has been ";
	
}
