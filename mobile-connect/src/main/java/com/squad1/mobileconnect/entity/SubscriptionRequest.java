package com.squad1.mobileconnect.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SubscriptionRequest {
	@Id
	private String requestId;
	@ManyToOne
	private Customer customer;
	@ManyToOne
	private SubscriptionPlan subscriptionPlan;
	@ManyToOne
	private NumberCatalog number;
	@Enumerated(EnumType.STRING)
	private Status status;
}
