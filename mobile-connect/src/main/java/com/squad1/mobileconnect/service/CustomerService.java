package com.squad1.mobileconnect.service;

import com.squad1.mobileconnect.dto.CustomerDto;
import com.squad1.mobileconnect.dto.SubscriptionResponse;

public interface CustomerService {

	SubscriptionResponse subscribePlans(Long subscriptionPlanId, Long numberId, CustomerDto customerDto);

}
