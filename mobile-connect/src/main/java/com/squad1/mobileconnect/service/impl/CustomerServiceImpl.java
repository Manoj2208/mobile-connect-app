package com.squad1.mobileconnect.service.impl;

import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.squad1.mobileconnect.dto.ApiResponse;
import com.squad1.mobileconnect.dto.CustomerDto;
import com.squad1.mobileconnect.dto.SubscriptionResponse;
import com.squad1.mobileconnect.entity.Customer;
import com.squad1.mobileconnect.entity.NumberCatalog;
import com.squad1.mobileconnect.entity.Status;
import com.squad1.mobileconnect.entity.SubscriptionPlan;
import com.squad1.mobileconnect.entity.SubscriptionRequest;
import com.squad1.mobileconnect.exception.CustomerConflictExists;
import com.squad1.mobileconnect.exception.NumberAlreadyTaken;
import com.squad1.mobileconnect.exception.NumberNotFound;
import com.squad1.mobileconnect.exception.SubscriptionPlanNotFound;
import com.squad1.mobileconnect.repository.CustomerRepository;
import com.squad1.mobileconnect.repository.NumberCatalogRepository;
import com.squad1.mobileconnect.repository.SubscriptionPlanRepository;
import com.squad1.mobileconnect.repository.SubscriptionRequestRepository;
import com.squad1.mobileconnect.service.CustomerService;
import com.squad1.mobileconnect.util.SuccessResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class CustomerServiceImpl implements CustomerService {
	private final CustomerRepository customerRepository;
	private final SubscriptionRequestRepository subscriptionRequestRepository;
	private final NumberCatalogRepository numberCatalogRepository;
	private final SubscriptionPlanRepository subscriptionPlanRepository;

	@Override
	@Transactional
	public SubscriptionResponse subscribePlans(Long subscriptionPlanId, Long numberId, CustomerDto customerDto) {

		SubscriptionPlan subscriptionPlan = subscriptionPlanRepository.findById(subscriptionPlanId).orElseThrow(() -> {
			log.error("Invalid subscriptionPlanId:" + subscriptionPlanId);
			throw new SubscriptionPlanNotFound();
		});

		NumberCatalog numberCatalog = numberCatalogRepository.findById(numberId).orElseThrow(() -> {
			log.error("Invalid numberId:" + numberId);
			throw new NumberNotFound();
		});

		if (!(numberCatalog.isAvailability()))
			throw new NumberAlreadyTaken();

		Optional<Customer> customer = customerRepository.findByAadharOrEmail(customerDto.aadhar(), customerDto.email());
		if (customer.isPresent()) {
			log.error("customer already exists:" + customerDto.email());
			throw new CustomerConflictExists();
		}

		Customer customer2 = Customer.builder().aadhar(customerDto.aadhar()).city(customerDto.city())
				.email(customerDto.email()).name(customerDto.userName()).build();
		log.info("customer information saved");
		customerRepository.save(customer2);

		String requestId = UUID.randomUUID().toString();
		SubscriptionRequest subscriptionRequest = SubscriptionRequest.builder().customer(customer2)
				.number(numberCatalog).requestId(requestId).subscriptionPlan(subscriptionPlan).status(Status.PENDING)
				.build();

		subscriptionRequestRepository.save(subscriptionRequest);

		log.info("Subscription request for the customer created");
		return SubscriptionResponse.builder().apiResponse(ApiResponse.builder()
				.httpStatus(SuccessResponse.SUCCESS_CODE1).message(SuccessResponse.SUCCESS_MESSAGE1).build())
				.requestId(requestId).build();
	}

}
