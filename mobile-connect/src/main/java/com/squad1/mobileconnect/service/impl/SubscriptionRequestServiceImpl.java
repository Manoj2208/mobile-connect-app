package com.squad1.mobileconnect.service.impl;

import org.springframework.stereotype.Service;

import com.squad1.mobileconnect.dto.ApiResponse;
import com.squad1.mobileconnect.dto.EmailTemplate;
import com.squad1.mobileconnect.entity.Status;
import com.squad1.mobileconnect.entity.SubscriptionRequest;
import com.squad1.mobileconnect.exception.InvalidAction;
import com.squad1.mobileconnect.exception.SubscriptionRequestNotFound;
import com.squad1.mobileconnect.kafka_producer.ProducerService;
import com.squad1.mobileconnect.repository.SubscriptionRequestRepository;
import com.squad1.mobileconnect.service.SubscriptionRequestService;
import com.squad1.mobileconnect.util.SuccessResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class SubscriptionRequestServiceImpl implements SubscriptionRequestService {

	private final SubscriptionRequestRepository subscriptionRequestRepository;

	private final ProducerService producerService;

	@Override
	public ApiResponse approveRequest(String requestId, Status status) {
		SubscriptionRequest subscriptionRequest = subscriptionRequestRepository.findById(requestId).orElseThrow(() -> {
			log.error("Invalid subscriptionRequest:" + requestId);
			throw new SubscriptionRequestNotFound();
		});

		if (status.equals(Status.PENDING)) {
			log.error("Invalid action on requested service");
			throw new InvalidAction();
		}

		subscriptionRequest.setStatus(status);
		subscriptionRequestRepository.save(subscriptionRequest);

		EmailTemplate emailTemplate = EmailTemplate.builder().subject(SuccessResponse.SUBJECT)
				.body(SuccessResponse.BODY + status).to(subscriptionRequest.getCustomer().getEmail()).build();
		log.info("event created to produce in kafka topic");
		producerService.send(emailTemplate);

		log.info("subscription Request for the customer updated");
		return ApiResponse.builder().message(SuccessResponse.SUCCESS_MESSAGE2).httpStatus(SuccessResponse.SUCCESS_CODE2)
				.build();
	}

}
