package com.squad1.mobileconnect.service;

import com.squad1.mobileconnect.dto.ApiResponse;
import com.squad1.mobileconnect.entity.Status;

public interface SubscriptionRequestService {

	ApiResponse approveRequest(String requestId, Status status);

}
