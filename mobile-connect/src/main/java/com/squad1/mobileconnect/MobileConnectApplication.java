package com.squad1.mobileconnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobileConnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobileConnectApplication.class, args);
	}

}
