package com.squad1.mobileconnect.dto;

import lombok.Builder;


@Builder
public record ApiResponse(String message,String httpStatus) {

}
