package com.squad1.mobileconnect.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;

public record CustomerDto(@NotBlank(message = "userName is required") String userName,
		@NotBlank(message = "email is required") @Email(message = "invalid email") String email,
		@NotBlank(message = "aadhar is required") @Pattern(regexp = "[1-9][\\d]{11}", message = "invalid aadhar") String aadhar,
		@NotBlank(message = "city is required") String city) {

}
