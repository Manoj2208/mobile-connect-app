package com.squad1.mobileconnect.dto;

import lombok.Builder;

@Builder
public record EmailTemplate(String subject,String body,String to) {

}
