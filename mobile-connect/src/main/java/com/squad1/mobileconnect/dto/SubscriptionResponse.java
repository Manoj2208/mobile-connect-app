package com.squad1.mobileconnect.dto;

import lombok.Builder;

@Builder
public record SubscriptionResponse(ApiResponse apiResponse,String requestId) {

}
