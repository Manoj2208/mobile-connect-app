package com.squad1.mobileconnect.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.squad1.mobileconnect.dto.ApiResponse;
import com.squad1.mobileconnect.entity.Status;
import com.squad1.mobileconnect.service.SubscriptionRequestService;
import com.squad1.mobileconnect.util.SuccessResponse;

@ExtendWith(SpringExtension.class)
class SubscriptionRequestControllerTest {
	@Mock
	private SubscriptionRequestService requestService;

	@InjectMocks
	private SubscriptionRequestController subscriptionRequestController;

	@Test
	void testSubscriptionRequestController() {
		ApiResponse apiResponse = ApiResponse.builder().httpStatus(SuccessResponse.SUCCESS_CODE2)
				.message(SuccessResponse.SUCCESS_MESSAGE2).build();
		Mockito.when(requestService.approveRequest("request1", Status.ENABLECONNECTION)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity = subscriptionRequestController.approveServiceRequest("request1",
				Status.ENABLECONNECTION);
		assertNotNull(responseEntity);
		assertEquals("SUCCESS02", responseEntity.getBody().httpStatus());

	}

}
