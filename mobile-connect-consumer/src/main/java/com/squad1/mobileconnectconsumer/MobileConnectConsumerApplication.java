package com.squad1.mobileconnectconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobileConnectConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobileConnectConsumerApplication.class, args);
	}

}
