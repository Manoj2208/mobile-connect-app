package com.squad1.mobileconnectconsumer.dto;

import lombok.Builder;

@Builder
public record EmailTemplate(String subject,String body,String to) {

}
