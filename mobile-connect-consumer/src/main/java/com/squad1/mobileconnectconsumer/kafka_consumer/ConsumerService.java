package com.squad1.mobileconnectconsumer.kafka_consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ConsumerService {

	@KafkaListener(topics = "notifier-queue", groupId = "group-id0")
	public void sendSms(ConsumerRecord<String, Object> emailTemplate) {
		log.info("" + emailTemplate.value());
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = null;
		try {
			node = mapper.readTree(emailTemplate.value().toString());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		String toMail = node.get("to").asText();
		String subject = node.get("subject").asText();
		String body = node.get("body").asText();
		log.info("" + toMail + "" + subject + "" + body);
	}
}
